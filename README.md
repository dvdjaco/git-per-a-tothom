# Git per a tothom
Aquest taller és una introducció per a persones sense cap experiència prèvia amb Git.

## Què és Git?
És un sistema de control de versions.
No requereix un servidor central (tot i que normalment el té).
Té un històric "no linial", es fan branques. Hi ha versions que divergeixen

## Per a què em pot servir?

## Git en acció
Per a crear col·laborativment, gestionar tasques, documentació, etc.
## Links
https://gitlab.com/dvdjaco/git-per-a-tothom/

